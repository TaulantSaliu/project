# Localhost tunneln mit ngrok

## Inhaltsverzeichnis
1. [Autoren, Versionierung des Dokumentes](#autoren)
2.  [Vorwort](#vorwort)
3. [Funktion des Services](#funktion)
	2.1. [Zusatz](#zusatz)
4. [Benötigte Hard- und Software](#ware)
	3.1. [Hardware](#hardware)
	3.2. [Software](#software)
5. [Installationsanleitung](#anleitung)
6. [Qualitätskontrolle](#qualitätskontrolle)
7. [Quiz](#quiz)
8. [Error-Handling](#error-handling)

## Autoren, Versionierung des Dokumentes <a name="autoren"></a>
*Version:* 1.0
*Autoren:* Taulant Saliu, Trojan Veseli

## Vorwort<a name="vorwort"></a>

Das Ziel dieses Dokuments ist, dass du erste Erfahrungen mit ngrok machst. Du wirst mit einem Beispiel aus der Realität konfrontiert und kannst das Thema mit einem kleinen Quiz abschliessen.

## Funktion des Services <a name="funktion"></a>
Was ist ein Web Tunnel und wozu brauchen wir ngrok?


Mit einem Web Tunnel kann man einen (Reverse-Proxy) einbauen, um dann somit zum Beispiel auf den RasPi Zugriff zu erhalten. Dies kann man sich so vorstellen, wie wenn ein Inhaftierter durch einen Tunnel aus dem Knast versucht zu fliehen.<br> Beispiel [El Chapo](https://www.spiegel.de/panorama/justiz/joaquin-guzman-el-chapo-floh-durch-diesen-tunnel-a-1043339.html)

"ngrok" ist in ein Service, welches eine Verbindung zum ngrok-Clouddienst herstellt. Es akzeptiert öffentlichen Datenverkehr und leitet diese durch den ngrok-Prozess an die angegebene lokale Adresse weiter. - In unserem El Chapo Beispiel wären es die Werkzeuge und Personen, die ihn dabei geholfen hatten.


### Zusatz<a name="zusatz"></a>
Der Service "*ngrok*" wird von guten Firewalls blockiert und ist somit nicht zugänglich.

*Fazit:* 

Man kann nicht aus jedem Gefängnis auf gleicher Art und Weise ausbrechen.

## Benötigte Hard- und Software <a name="ware"></a>

### Hardware<a name="hardware"></a>
- Aufgesetzter RasPi
- Handy (3G, 4G)

### Software<a name="software"></a>
- Linux für Raspberry (Raspbian...)
- [ngrok (Linux ARM 64)](https://ngrok.com/download)




## Installationsanleitung <a name="anleitung"></a>
1. Über oben genannten Link ngrok für Linux installieren.
2. ngrok Datei ins Verzeichnis ~/ngrok/ auf dem RasPi kopieren.
3. ngrok über folgende Commands auf RasPi starten:<br> &nbsp;&nbsp;&nbsp;&nbsp;`cd`<br>
&nbsp; &nbsp;&nbsp;`cd ./ngrok`<br>
&nbsp;&nbsp;&nbsp;&nbsp;`./ngrok"Protokollabkürzung""Protokollnummer"` *[Info]*
4. Nun kann man über  `http://"IpAdresse".ngrok.io/file.html`  auf den RasPi zugreifen.

*[Info]* = Sie müssen auf eine Website zugreifen und benötigen dazu den entsprechen Protokoll (Eingangstür).


## Qualitätskontrolle<a name="qualitätskontrolle"></a>
Der Service kann nun getestet werden, indem man ein File erstellt und mit dem oben genannten Link darauf zugreifen kann.


## Quiz<a name="quiz"></a>
 [Webtunnel Ngrok Quiz](https://www.goconqr.com/de/p/20068315-Webtunnel-Ngrok-M126-quizzes)


## Error-Handling<a name="error-handling"></a>
Falscher Port -> (Richtiger wäre = 80)<br>
Falsche Version -> Aktuellste Version von Ngrok installieren<br>
Keine stabile Internetverbindung -> RasPi auf Internetverbindung prüfen







<!--stackedit_data:
eyJoaXN0b3J5IjpbMTMwNDE5MDU3MiwtMTk2MTY0MDE1MywxND
g3ODI3MTM2LC0xODM3MjE5MDI5LDkzMzk3NjA0M119
-->